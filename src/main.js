import Vue from 'vue';
import Vuetify from 'vuetify';
import VueResource from 'vue-resource';
import App from './App.vue';
import VueClip from 'vue-clip';
import {
  store
} from './store.js';

Vue.use(Vuetify);
Vue.use(VueResource);
Vue.http.interceptors.push((request, next) => {
  request.headers.set('Authorization', 'Token 6e221994c946d5431659fbac1e6ca0694ebfd1fc');
  request.headers.set('Accept', 'application/json');
  next();
});

Vue.use(VueClip);

new Vue({
  el: '#app',
  store,
  render: h => h(App)
});