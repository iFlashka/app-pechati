import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    sesion_info: {
      price: [],
      currentStep: '',
      prevStep: '',
      steps: []
    },
    data: [],
    sesion_data: {}
  },
  mutations: {
    CHANGE_STEP(state, {
      currentStep,
      prevStep,
      stepName,
      action = 'setStep'
    }) {
      if (action == 'setStep') {
        state.sesion_info.currentStep = currentStep;
        state.sesion_info.prevStep = prevStep;
        for (let i in state.sesion_info.steps) {
          if (state.sesion_info.steps[i].prevStep == prevStep) {
            state.sesion_info.steps.splice(i, state.sesion_info.steps.length);
          }
        }
        state.sesion_info.steps.push({
          currentStep,
          prevStep,
          stepName
        });
      } else if (action == 'changeStep') {
        state.sesion_info.currentStep = currentStep;
        state.sesion_info.prevStep = prevStep;
      }
    },
    CHANGE_ACTIVE_ITEM(state, {
      value,
      step,
      price
    }) {
      Vue.set(state.sesion_info, step, value);
      if (price != null) return Vue.set(state.sesion_info, `${step}_price`, price);
    },
    GET_DATA(state, data) {
      state.data = data;
    },
    SAVE_DATA(state, {
      to,
      data
    }) {
      state.sesion_info[to] = data;
    },
    GET_COMPONENT_DATA(state, data) {
      let param = state.sesion_info[data];
      // console.log(param);
      let thisComponent = state.sesion_info.currentStep;
      let arr = state.data[thisComponent];
      let filtred = arr.filter((item, i) => {
        for (let relation of item.relations) {
          if (relation === param) return true;
        }
      });
      state.sesion_data[thisComponent] = filtred;
    }
  },
  getters: {
    steps: state => state.sesion_info.steps.filter(step => step.stepName != undefined),
    countPrice: state => (state.sesion_info.print_list_price ? state.sesion_info.print_list_price : 0) + (state.sesion_info.snap_price ? state.sesion_info.snap_price : 0)
  },
  actions: {
    changeItem({
      commit
    }, data) {
      console.log(data);
      commit('CHANGE_ACTIVE_ITEM', data);
    },
    changeStep({
      commit
    }, step) {
      commit('CHANGE_STEP', step);
    },
    getData({
      commit
    }, data) {
      commit('GET_DATA', data);
    },
    getComponentData({
      commit
    }, data) {
      commit('GET_COMPONENT_DATA', data);
    },
    saveData({
      commit
    }, data) {
      commit('SAVE_DATA', data);
    }
  },
});

export {
  store
};