export default {
  methods: {
    getComponentData(value) {
      this.$store.dispatch('getComponentData', value);
    }
  }
};