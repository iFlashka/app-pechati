export default {
  data() {
    return {
      snackbar: false,
      text: ''
    };
  },
  methods: {
    toast(name) {
      this.snackbar = true;
      this.text = `Выбрана: ${name}`;
    }
  }
};