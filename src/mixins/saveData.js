export default {
  methods: {
    saveData(to, data) {
      this.$store.dispatch('saveData', {
        data, to
      });
    }
  }
};