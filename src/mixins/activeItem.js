export default {
  computed: {
    currentStep() {
      return this.$store.state.sesion_info.currentStep;
    },
    prevStep() {
      return this.$store.state.sesion_info.prevStep;
    }
  },
  methods: {
    checkChangeData(step, name) {
      if(this.$store.state.sesion_info[step] == name && this.$store.state.sesion_info[step] !== undefined) {
        return 'changeStep';
      } else {
        return 'setStep';
      }
    },
    steps(currentStep, prevStep = '', stepName, action) {
      setTimeout(() => {
        this.$store.dispatch('changeStep', {
          currentStep, prevStep, stepName, action
        });
      }, 300);
    },
    changeItem(value, step, price = null) {
      this.$store.dispatch('changeItem', {
        value, step, price
      });
    }
  }
};